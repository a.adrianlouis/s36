const mongoose = require("mongoose")

const task_schema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "Pending"
	}
})

module.exports = mongoose.model("Task", task_schema)