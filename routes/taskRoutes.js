const express = require("express")
const router = express.Router()
const TaskController = require("../controllers/TaskController")

// Get all tasks
router.get("/", (request, response) => {
	TaskController.getAll().then(result => response.send(result))
})

// Get specific task
router.get("/:id", (request, response) => {
	TaskController.getSpecificTask(request.params.id).then(result => response.send(result))
})

//========================= Create new task
router.post("/", (request, response) => {
	TaskController.createTask(request.body).then(result => response.send(result))
})

//========================= Update existing task
router.put("/:id/complete", (request, response) => {
	TaskController.updateTask(request.params.id, request.body).then(result =>
		response.send(result))
})



module.exports = router